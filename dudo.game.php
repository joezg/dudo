<?php
 /**
  *------
  * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
  * Dudo implementation : © Jurica Hladek <jurica.hladek@gmail.com>
  *
  * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
  * See http://en.boardgamearena.com/#!doc/Studio for more information.
  * -----
  *
  * dudo.game.php
  *
  * This is the main file for the game logic.
  *
  * In this PHP file are defined rules of the game.
  *
  */


require_once( APP_GAMEMODULE_PATH.'module/table/table.game.php' );

class Dudo extends Table
{
    //currentRoundType
    const NORMAL_ROUND = 0;
    const PALIFICO_ROUND = 1;

    //calzaVariant
    const CALZA_YES = 2;

    //lastCall
    const CALL_DUDO = 0;
    const CALL_CALZA = 1;
    const CALL_ELIMINATION = 2;

	function __construct( )
	{


        //  Global variables labels:
        //  Labels are assigned to global variables used for this game.
        //  IDs must be any number between 10 and 99.
        //  If the game has options (variants), label is associated here to
        //  the corresponding ID in gameoptions.inc.php.
         parent::__construct();self::initGameStateLabels( array(
                    "currentRoundType" => 10,  //normal round (0) or palifico (1).
					"currentBid" => 11,
					"currentBidPlayerId" =>12,
					"playerWhoLostId" =>13,
                    "palificoDieFace" =>14,
                    "lastCall" =>15,
                    "isCalzaSuccess" =>16,
                    "calzaVariant" => 100,
        ) );

	}

    protected function getGameName( )
    {
        return "dudo";
    }

    /*
        setupNewGame:

        This method is called only once, when a new game is launched.
        In this method, the game is setup according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame( $players, $options = array() )
    {
         $sql = "DELETE FROM player WHERE 1 ";
         self::DbQuery( $sql );

         // Set the colors of the players with HTML color code
         // The default below is red/green/blue/orange/brown
         // The number of colors defined here must correspond to the maximum number of players allowed for the gams
         $default_colors = array( "ff0000", "008000", "0000ff", "ffa500", "a826d7", "ffff00");

         // Create players
         $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar, player_score) VALUES ";
         $values = array();
         foreach( $players as $player_id => $player )
         {
             $color = array_shift( $default_colors );
             $values[] = "('".$player_id."','$color','".$player['player_canal']."','".addslashes( $player['player_name'] )."','".addslashes( $player['player_avatar'] )."', 0)";
         }
         $sql .= implode( $values, ',' );
         self::DbQuery( $sql );
         self::reattributeColorsBasedOnPreferences( $players, array( "ff0000", "008000", "0000ff", "ffa500", "a826d7", "ffff00" ) );
         self::reloadPlayersBasicInfos();

         // self::DbQuery("UPDATE player SET player_dice_nr = 2");

        /************ Start the game initialization *****/
		 // Init global values with their initial values
         self::setGameStateInitialValue( 'currentRoundType', self::NORMAL_ROUND );
		 self::setGameStateInitialValue( 'currentBid', 0 );
		 self::setGameStateInitialValue( 'currentBidPlayerId', 0 );
		 self::setGameStateInitialValue( 'playerWhoLostId', 0 );
         self::setGameStateInitialValue( 'palificoDieFace', 0 );
         self::setGameStateInitialValue( 'lastCall', -1 );
         self::setGameStateInitialValue( 'isCalzaSuccess', 0 );

         // Init game statistics
         self::initStat( 'table', 'rounds_number', 0 );
         self::initStat( 'table', 'ended_with_calza', 0 );
         self::initStat( 'table', 'ended_with_dudo', 0 );
         self::initStat( 'player', 'turns_number', 0 );
         self::initStat( 'player', 'number_of_dice_rolled', 0 );
         self::initStat( 'player', 'number_of_wild_dice_rolled', 0 );
         self::initStat( 'player', 'number_of_dudo_calls', 0 );
         self::initStat( 'player', 'number_of_successful_dudo_calls', 0 );
         self::initStat( 'player', 'number_of_calza_calls', 0 );
         self::initStat( 'player', 'number_of_successful_calza_calls', 0 );
         self::initStat( 'player', 'rounds_number', 0 );
         self::initStat( 'player', 'rounds_with_second_bid', 0 );

         // Activate first player (which is in general a good idea :) )
         $this->activeNextPlayer();
         /************ End of the game initialization *****/
    }

    /*
        getAllDatas:

        Gather all informations about current game situation (visible by the current player).

        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
		$result = array( 'players' => array(), 'currentPlayerDice' => array() );

        $current_player_id = self::getCurrentPlayerId();    // !! We must only return informations visible by this player !!

        // Get information about players
        $sql = "SELECT player_id id, player_score score, player_last_bid, isEliminated, player_dice_nr FROM player ";
        $result['players'] = self::getCollectionFromDb( $sql );

        //Gather all information about current game situation (visible by player $current_player_id).
        $sql = "SELECT player_die1 die_1, player_die2 die_2, player_die3 die_3, player_die4 die_4, player_die5 die_5 FROM player WHERE player_id = $current_player_id";
        $result['currentPlayerDice'] = self::getObjectFromDB( $sql );

            //Gather all other data
		$result['currentBid'] = self::getGameStateValue( 'currentBid' );

		$result['totalNumberOfDiceEstimate'] = $this->getDiceNrEstimate();

		$result['minimumBids'] = $this->getMinimumBids();
        $result['maximumBids'] = $this->getMaximumBids();

        $result['roundType'] = self::getGameStateValue( 'currentRoundType');

        return $result;
    }

    /*
        getGameProgression:

        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).

        This method is called each time we are in a game state with the "updateGameProgression" property set to true
        (see states.inc.php)
    */
    function getGameProgression()
    {
		$nr = self::getPlayersNumber();

		switch ($nr) {
			case 2:
				$nrDice = 7;
				break;
			case 3:
				$nrDice = 12;
				break;
			case 4:
				$nrDice = 17;
				break;
			case 5:
				$nrDice = 22;
				break;
			case 6:
				$nrDice = 27;
				break;
		}

		$sql = "SELECT SUM(player_dice_nr) allDice , MAX(player_dice_nr) maxDie FROM player";
        $dice = self::getObjectFromDB( $sql );

		$min = $dice['allDice'] - $dice['maxDie'];
		$max = $dice['allDice'] - 1;

		$avg = ($min + $max) / 2;

        return intval( ($nrDice - $avg) / $nrDice * 100 );
    }


//*****************************************************************************************************
//**             Utility functions
//*****************************************************************************************************

    /**
     * @param $currentPlayerId
     * @return array
     */
    public function getCalzaPlayers()
    {
        //$currentPlayerId = self::getActivePlayerId();
        $lastPlayerId = self::getGameStateValue('currentBidPlayerId');

        if ($lastPlayerId == 0)
            return array();

        $sql = "
              SELECT player_id
              FROM player
              WHERE (player_dice_nr > 0
              AND player_id <> $lastPlayerId)";
        //OR player_id = $currentPlayerId";

        $calzaPlayersIds = array_keys(self::getCollectionFromDb($sql));
        return $calzaPlayersIds;
    }

    public function getPlayerNameWithColor($name, $color)
    {
        return '<span style="font-weight:bold;color:#'.$color.';">'.$name.'</span>';
    }

    public function getDieClass($bidDie)
    {
        $dieClass = "";
        switch ($bidDie) {
            case 1:
                $dieClass = "die_one";
                break;
            case 2:
                $dieClass = "die_two";
                break;
            case 3:
                $dieClass = "die_three";
                break;
            case 4:
                $dieClass = "die_four";
                break;
            case 5:
                $dieClass = "die_five";
                break;
            case 6:
                $dieClass = "die_six";
                break;
        }
        return $dieClass;
    }

    public function getDieTitle($bidDie)
    {
        $dieTitle = "";
        switch ($bidDie) {
            case 1:
                $dieTitle = "Paco";
                break;
            case 2:
                $dieTitle = "Two";
                break;
            case 3:
                $dieTitle = "Three";
                break;
            case 4:
                $dieTitle = "Four";
                break;
            case 5:
                $dieTitle = "Five";
                break;
            case 6:
                $dieTitle = "Six";
                break;
        }
        return $dieTitle;
    }

    function adjustDotsColor($playerColor){
        $adjusted = $playerColor;

        switch ($playerColor){
            case "colors_with_black_dots":
                break;
            default:
                $adjusted .= ' white-dots';
        }

        return $adjusted;
    }

    public function getLastBid()
    {
        $bid = self::getGameStateValue('currentBid');
        $lastBid = array();
        $lastBid["die"] = $bid % 10;
        $lastBid["value"] = (int)($bid / 10);
        return $lastBid;
    }

    public function checkTotalDiceNumber($lastBid)
    {
        $dieFaceColumn = "";
        switch ($lastBid["die"]) {
            case 1:
                $dieFaceColumn = "nbrOnes";
                break;
            case 2:
                $dieFaceColumn = "nbrTwos";
                break;
            case 3:
                $dieFaceColumn = "nbrThrees";
                break;
            case 4:
                $dieFaceColumn = "nbrFours";
                break;
            case 5:
                $dieFaceColumn = "nbrFives";
                break;
            case 6:
                $dieFaceColumn = "nbrSixes";
                break;

        }

        $sql = 'select sum(' . $dieFaceColumn . ')';

        if ($lastBid["die"] != 1 && self::getGameStateValue('palificoDieFace') == 0)
            $sql .= ' + sum(nbrOnes) ';

        $sql .= 'from player';
        $nr = self::getUniqueValueFromDB($sql);
        return $nr;
    }

    //Gets the next player by skipping any eliminated player
    //Code is taken and is similar to code in Hawaii
    function getNextPlayer()
    {
        //$current_player = self::getActivePlayerId();
        $current_player = self::getGameStateValue( 'currentBidPlayerId');

        // Build player eliminated array
        $player_eliminated = array();
        $player_zombie = array();
        $noActivePlayers = 0;
        $sql = "SELECT player_id, player_name, player_color, isEliminated, player_last_bid, player_zombie FROM player";
        $players = self::getCollectionFromDb( $sql );

        foreach ($players as $player)
        {
            $player_eliminated[ $player['player_id'] ] = $player['isEliminated'] == 1;
            $player_zombie[ $player['player_id'] ] = $player['player_zombie'] == 1;
            if ( $player_eliminated[ $player['player_id'] ] == false && $player_zombie[ $player['player_id'] ] == false)
                $noActivePlayers++;
        }

        if( $noActivePlayers > 1 )
        {
            $next_player = self::getNextPlayerTable();

            while( true )
            {
                $current_player = $next_player[ $current_player ];

                if( $player_eliminated[ $current_player ] )
                {
                    // This player is eliminated => we can't choose him
                }
                else if ($player_zombie[ $current_player ])
                {
                    self::notifyAllPlayers( "zombie_pass", clienttranslate( '${name} is skipped'), array(
                        "name" => $this->getPlayerNameWithColor($players[$current_player]['player_name'], $players[$current_player]['player_color'])
                    ) );

                }
                else
                {
                    if ($players[$current_player]["player_last_bid"] != null)
                        self::incStat(1, "rounds_with_second_bid", $current_player);
                    // We found our next player
                    return $current_player;
                }
            }
        }

        return -1;
    }

    function skipEliminatedAndActivate(){
        $next_id = $this->getNextPlayer();

        if (-1 == $next_id)
            return false;

        $this->gamestate->changeActivePlayer( $next_id );

        return true;
    }

	function getDiceNrEstimate()
	{
		$sql = "SELECT sum(player_dice_nr) FROM player";
		$diceNrEst = self::getUniqueValueFromDB( $sql ); //here, the total number of dice is estimated, so not to send accurate number to UI which is not allowed by the rules
		//$diceNrEst = (int)($diceNrEst/5);
		//$diceNrEst = $diceNrEst * 5;
        return $diceNrEst;

	}

    // Return players => direction (L/TL/TR/R/BR/BL) from the point of view
    //  of current player (current player must be on BL)
    function getPlayersToDirection()
    {
        $result = array();

        $players = self::loadPlayersBasicInfos();
        $nextPlayer = self::createNextPlayerTable( array_keys( $players ) );

        $current_player = self::getCurrentPlayerId();

        switch (count($players)) {
            case 2:
                $directions = array( 'BL', 'TL' );
                break;
            case 3:
                $directions = array( 'BL', 'L', 'TL' );
                break;
            case 4:
                $directions = array( 'BL', 'TL', 'TR', 'BR');
                break;
            case 5:
                $directions = array( 'BL', 'L', 'TL', 'TR', 'BR' );
                break;
            case 6:
                $directions = array( 'BL', 'L', 'TL', 'TR', 'R', 'BR' );
                break;
        }

        if( ! isset( $nextPlayer[ $current_player ] ) )
        {
            // Spectator mode: take any player for BL
            $player_id = $nextPlayer[0];
            $result[ $player_id ] = array_shift( $directions );
        }
        else
        {
            // Normal mode: current player is on BL
            $player_id = $current_player;
            $result[ $player_id ] = array_shift( $directions );
        }

        while( count( $directions ) > 0 )
        {
            $player_id = $nextPlayer[ $player_id ];
            $result[ $player_id ] = array_shift( $directions );
        }
        return $result;
    }

    //Roll dice and clear last bid
    function initPlayer($playerId)
    {
        $sql = "SELECT player_dice_nr, isEliminated FROM player WHERE player_id = $playerId";

        $player_info = self::getObjectFromDB( $sql );
        $nr = $player_info['player_dice_nr'];

        $isEliminated = $player_info['isEliminated'] == 1;
        if (!$isEliminated)
            self::incStat(1, "rounds_number", $playerId);

        $dice = array( 'die_1' => 'NULL', 'die_2' => 'NULL', 'die_3' => 'NULL', 'die_4' => 'NULL', 'die_5' => 'NULL');

		$ones = 0;
		$twos = 0;
		$threes = 0;
		$fours = 0;
		$fives = 0;
		$sixes = 0;
		for ($i = 1; $i <= $nr; $i++)
        {
            $dice['die_'.$i] = bga_rand(1,6);

			switch ($dice['die_'.$i]) {
				case 1:
					$ones++;
					break;
				case 2:
					$twos++;
					break;
				case 3:
					$threes++;
					break;
				case 4:
					$fours++;
					break;
				case 5:
					$fives++;
					break;
				case 6:
					$sixes++;
					break;

			}
        }

        self::incStat($nr, 'number_of_dice_rolled', $playerId);
        if (self::getGameStateValue( 'currentRoundType') == self::NORMAL_ROUND)
            self::incStat($ones, 'number_of_wild_dice_rolled', $playerId);

        $sql = "UPDATE player SET player_die1 = ".$dice['die_1'].", player_die2 = ".$dice['die_2'].", player_die3 = ".$dice['die_3'].", player_die4 = ".$dice['die_4'].", player_die5 = ".$dice['die_5'].", player_last_bid = NULL";
		$sql = $sql.", nbrOnes = ".$ones.", nbrTwos = ".$twos.", nbrThrees = ".$threes.", nbrFours = ".$fours.", nbrFives = ".$fives.", nbrSixes = ".$sixes." WHERE player_id = $playerId";
        self::DbQuery($sql);

		$playerState = array( 'dice_nr' => $nr,
							'last_bid' => null,
							'dice' => $dice);

		return $playerState;
    }

	function getMinimumBids()
	{
        $isPalifico = self::getGameStateValue( 'currentRoundType') == 1;

		$bid = self::getGameStateValue( 'currentBid');
		$lastBid = $this->getLastBid();

        $bids = array();

        //when palifico and not first bid return only last bid die
        if ($isPalifico && (int)$bid != 0){
            $bids[$lastBid["die"]] = $lastBid["value"] + 1;
            return $bids;
        }

        //add all die faces larger or equal of las bid die
		for ($i=2; $i<=6; $i++) {
            if ($i>= $lastBid['die'])
                $bids[$i] = $this->getMinimumBid($lastBid, $i);
        }

        //add paco if it is palifico round and first bid (if it is not the function will return earlier)
        //          - you can start with paco only in palifico
        //or not first bid - you can always switch to paco unless in palifico
        if ($isPalifico || (int)$bid != 0)
            $bids[1] = $this->getMinimumBid($lastBid, 1);

		return $bids;
	}

    function checkMaximumBid($bid){
        $die = $bid['die'];
        $value = $bid['value'];

        $maxBids = $this->getMaximumBids();
        $maxValue = $maxBids[$die];

        if ($maxValue == -1)
            return true;
        else
            return $value<=$maxValue;
    }

    function getMaximumBids()
    {
        $isPalifico = self::getGameStateValue( 'currentRoundType') == 1;

        $bid = self::getGameStateValue( 'currentBid');
        $lastBid = $this->getLastBid();

        $bids = array();

        //when palifico and not first bid return only last bid die
        if ($isPalifico && (int)$bid != 0){
            $bids[$lastBid["die"]] = -1;
            return $bids;
        }

        //add all die faces larger or equal of las bid die
        for ($i=2; $i<=6; $i++) {
            if ($i == $lastBid['die'] || (int)$bid == 0 || $lastBid['die'] == 1)
                $bids[$i] = -1;
            else if ($i > $lastBid['die'])
                $bids[$i] = $lastBid["value"];

        }

        //add paco if it is palifico round and first bid (if it is not the function will return earlier)
        //          - you can start with paco only in palifico
        //or not first bid - you can always switch to paco unless in palifico
        if ($isPalifico || (int)$bid != 0)
            $bids[1] = -1;

        return $bids;
    }

	function getMinimumBid($lastBid, $die)
	{
        $minValue = 1;

		if ($lastBid["die"] == 0 or $lastBid["value"] == 0)
			return $minValue;

		if ($die == 1)
		{
			if ($lastBid["die"] != 1)
			{
				$minValue = ceil($lastBid["value"]/2);
			}
			else
			{
				$minValue = $lastBid["value"] + 1;
			}
		}
		else
		{
			if ($lastBid["die"] == 1)
			{
				$minValue = $lastBid["value"] * 2 + 1;
			}
			else if ($die <= $lastBid["die"])
			{
				$minValue = $lastBid["value"] + 1;
			}
			else
				$minValue = $lastBid["value"];
		}

		return $minValue;
	}

    function generateScoreTable($playerWithDiceChange)
    {
        $lastCall = self::getGameStateValue('lastCall');

        $isCalza = $lastCall == self::CALL_CALZA;
        $isGiveUp = $lastCall == self::CALL_ELIMINATION;

        $isCalzaSuccess = false;
        if ($isCalza)
            $isCalzaSuccess = self::getGameStateValue('isCalzaSuccess') == 1;

        $table = array();

        $firstRow = array();
        $firstRow[] = array( 'str' => clienttranslate('player'),
            'args' => array( ),
            'type' => 'header'
        );
        $firstRow[] = array( 'str' => clienttranslate('dice'),
            'args' => array( ),
            'type' => 'header'
        );
        $firstRow[] = array( 'str' => clienttranslate('last bid'),
            'args' => array( ),
            'type' => 'header'
        );
        if ($isCalza)
            $gainLossColumnName = clienttranslate('die gained/lost');
        else
            $gainLossColumnName = clienttranslate('die lost');

        $firstRow[] = array( 'str' => $gainLossColumnName,
            'args' => array( ),
            'type' => 'header'
        );

        $table[] = $firstRow;

        $sql = "SELECT player_id, player_name, player_color, player_last_bid, player_die1, player_die2, player_die3, player_die4, player_die5 FROM player";
        $players = self::getCollectionFromDb( $sql );

        foreach( $players as $player_id => $player )
        {

            $playerRow = array( );
            $playerRow[] = array( 'str' => '${player_name}',
                'args' => array( 'player_name' => $player['player_name'] ),
                'type' => 'header'
            );

            $dice = array($player['player_die1'], $player['player_die2'], $player['player_die3'], $player['player_die4'], $player['player_die5']);
            $playerRow[] = $this->generateScoreDice($dice, $player['player_color']);

            if ($player_id == $this->getCurrentPlayerId())
            {
                if ($isCalza)
                {
                    $playerRow[] = array(
                        'str' => '<div class="die-score">${diescore}</div>',
                        'args' => array( 'i18n' => 'diescore', 'diescore' => clienttranslate( "Calza" ) )
                    );
                }
                else if ($isGiveUp)
                {
                    $playerRow[] = array(
                        'str' => '<div class="die-score">${diescore}</div>',
                        'args' => array( 'i18n' => 'diescore', 'diescore' => clienttranslate( "Give up" ) )
                    );
                }
                else
                {
                    $playerRow[] = array(
                        'str' => '<div class="die-score">${diescore}</div>',
                        'args' => array( 'i18n' => 'diescore', 'diescore' => clienttranslate( "Dudo" ) )
                    );
                }
           }
            else if ($player_id == self::getGameStateValue('currentBidPlayerId'))
                $playerRow[] = $this->generateScoreBid($player['player_last_bid'], $player['player_color'], true);
            else
                $playerRow[] = $this->generateScoreBid($player['player_last_bid'], $player['player_color'], false);

            if ($player_id == $playerWithDiceChange)
            {
                if ($isCalza)
                {
                    if ($isCalzaSuccess) {
                        $diceNr = 0;
                        foreach ($dice as $die)
                        {
                            if ($die != '') {
                                $diceNr++;
                            }
                        }

                        if ($diceNr == 5)
                            $playerRow[] = "<div class='die-score'>-</div>";
                        else
                            $playerRow[] = "<div class='die-score'>".clienttranslate("gained")."</div>";

                    }
                    else
                        $playerRow[] = "<div class='die-score'>".clienttranslate("lost")."</div>";
                } else {
                    $playerRow[] = "<div class='die-score'>X</div>";
                }
            }
            else
                $playerRow[] = '';

                $table[] = $playerRow;
        }

        $diceNumberRow = array();
        $lastBid = $this->getLastBid();
        $totalNumberOfDice = $this->checkTotalDiceNumber($lastBid);
        $diceNumberRow[]=array( 'str' => "<strong>".clienttranslate("Total number of checked dice")."</strong>",
            'args' => array( ),
            'type' => 'header'
        );
        $diceNumberRow[]="<div class='die-score'>".$totalNumberOfDice."</div>";
        $diceNumberRow[]='';
        $diceNumberRow[]='';
        $table[]=$diceNumberRow;

        return $table;
    }

    function generateScoreBid($bid, $color, $isLast)
    {
        $diceHTML = '';
        if ($bid != '')
        {
            $bidObj = array();
            $bidObj["die"] = $bid % 10;
            $bidObj["value"] = (int)($bid / 10);

            $last = "";
            if ($isLast)
                $last = " last-bid";

            $diceHTML = "<div class='dice-bid".$last."'><div class='die-score'>".$bidObj['value']."</div>";

            $dieClass = $this->getDieClass($bidObj["die"]);

            $diceHTML .= "<div class='die-score die die_".$this->adjustDotsColor($color)."'><div class='dots ".$dieClass."'></div></div>";
            $diceHTML .= "</div>";
        }
        return $diceHTML;
    }

    function generateScoreDice($dice, $color)
    {
        $diceHTML = "<div class='dice-score'>";

        $lastBidDie = $this->getLastBid();
        $lastBidDie = $lastBidDie['die'];
        $isPalifico = self::getGameStateValue( 'currentRoundType') == 1;

        $dieClass = "";
        foreach ($dice as $die)
        {
            if ($die != '')
            {
                $dieClass = $this->getDieClass($die);

                $dimmedClass = '';
                if ($die <> $lastBidDie)
                    $dimmedClass = 'score-die-dimmed';

                if ($die == 1 && !$isPalifico)
                    $dimmedClass = '';

                    $diceHTML .= "<div class='die-score ".$dimmedClass." die die_".$this->adjustDotsColor($color)."'><div class='dots ".$dieClass."'></div></div>";
            }
        }

        $diceHTML .= "</div>";

        return $diceHTML;
    }

    function completeElimination($player_id){
        self::DbQuery( "UPDATE player SET player_score=player_score+1 WHERE isEliminated = 0" );

        $sql = "SELECT player_id FROM player WHERE isEliminated = 0";
        $activePlayers = self::getCollectionFromDb( $sql );
        self::notifyAllPlayers( "score", '', array(
            'eliminatedPlayerId'=>$player_id,
            'activePlayers'=>$activePlayers,
        ) );
    }

    function eliminateZombies(){
        $sql = "SELECT player_id, player_name, player_color FROM player WHERE isEliminated = 0 AND player_zombie = 1";
        $zombiesToEliminate = self::getCollectionFromDb( $sql );

        self::DbQuery( "UPDATE player SET isEliminated=1, player_dice_nr = 0,
                                          player_die1 = null, player_die2 = null, player_die3 = null, player_die4 = null, player_die5 = null,
                                          nbrOnes = 0, nbrTwos = 0, nbrThrees = 0, nbrFours = 0, nbrFives = 0,
                                          player_last_bid = null
                                          WHERE isEliminated = 0 AND player_zombie = 1" );

        foreach($zombiesToEliminate as $id => $zombie)
        {
            self::notifyAllPlayers( "zombie_elimination", '${name} left the game and is eliminated.', array(
                'name' => $this->getPlayerNameWithColor($zombie['player_name'], $zombie['player_color'])
            ) );

            $this->completeElimination($id);
        }
    }

//****************************************************************************************
//**                Player actions
//****************************************************************************************

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in dudo.action.php)
    */

	function bid($bid)
    {
		// Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction( 'bid' );

        $player_id = self::getActivePlayerId();

        $minimumBids = $this->getMinimumBids();
        $maximumBids = $this->getMaximumBids();

        if (!array_key_exists($bid["die"], $minimumBids) || !array_key_exists($bid["die"], $maximumBids))
            throw new BgaUserException( str_replace( '$bid["die"]', $bid["die"], self::_('This bid is illegal! You cannot bid {$bid["die"]} at the moment!') ) );

		if ($bid["value"] < $minimumBids[$bid["die"]])
			throw new BgaUserException( self::_("This bid is not high enough!") );

        if (!$this->checkMaximumBid($bid))
            throw new BgaUserException( self::_("This bid is too high!") );

        $roundType = self::getGameStateValue('currentRoundType');
        if ($roundType == self::PALIFICO_ROUND)
        {
            $palificoFace = self::getGameStateValue('palificoDieFace');

            if ($palificoFace != 0 && $palificoFace != $bid['die'])
                throw new BgaUserException( self::_("You cannot change face value! It is palifico round.") );

            if ($palificoFace == 0){
                self::setGameStateValue('palificoDieFace', $bid['die']);
            }
        }

        $lastBid = $bid["die"] + $bid["value"]*10;

        $sql = "UPDATE player SET player_last_bid = $lastBid WHERE player_id = $player_id";
        self::DbQuery($sql);

		self::setGameStateValue( 'currentBid', $lastBid );
		self::setGameStateValue( 'currentBidPlayerId', $player_id );

        $players = $this->loadPlayersBasicInfos();

        $dotsClass = $this->getDieClass($bid["die"])."_notif";
        $dieTitle = $this->getDieTitle($bid["die"]);
		// Notify all players about the bid
        //self::notifyAllPlayers( "bid", clienttranslate( '${player_name} claims that ${nr_dice} dice shows ${face_die}'), array(
        self::notifyAllPlayers( "bid", clienttranslate( '${player_name} bids ${nr_dice} <div class="die-notif die_${color}" title="${title}"><div class="dots-notif ${dots_class}"></div></div>'), array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
            'nr_dice' => $bid["value"],
            'face_die' => $bid["die"],
            'dots_class' => $dotsClass,
            'color' => $this->adjustDotsColor($players[$player_id]['player_color']),
            'title' => $dieTitle,
        ) );

		$this->gamestate->nextState( "bid" );
    }

	function doubt()
    {
		// Check that this is the player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction( 'doubt' );

        $player_id = self::getActivePlayerId();

        self::incStat(1, "number_of_dudo_calls", $player_id);
        self::incStat(1, "ended_with_dudo");

        $lastBid = $this->getLastBid();

        $nr = $this->checkTotalDiceNumber($lastBid);

		if ($nr >= $lastBid["value"])
		{
			$msg = clienttranslate( '${player_name} called dudo and failed');
            $msgLoss = clienttranslate( '${player_name} loses a die');
			$playerToLose_id = $player_id;

		} else
		{
			$msg = clienttranslate('${player_name} called dudo and succeeded.');
            $msgLoss = clienttranslate('${playerToLose_name} loses a die');
			$playerToLose_id = self::getGameStateValue( 'currentBidPlayerId');
            self::incStat(1, "number_of_successful_dudo_calls", $player_id);

		}

		$sql = "select player_name, player_dice_nr, player_die1, player_color from player where player_id = ".$playerToLose_id;
		$playerToLose_info = self::getObjectFromDB( $sql );

        $sql = "update player set isEliminated = if (player_dice_nr = 1, 1, 0)";
        $sql .= ", player_dice_nr = player_dice_nr - 1 where player_id = ".$playerToLose_id;
		self::DbQuery($sql);

        self::setGameStateValue('lastCall', self::CALL_DUDO);

        // Notify all players about the bid
        self::notifyAllPlayers( "doubt", $msg, array(
            'player_id' => $player_id,
            'player_name' => self::getActivePlayerName(),
			'playerToLose_id' => $playerToLose_id,
			'playerToLose_name' => $this->getPlayerNameWithColor($playerToLose_info['player_name'], $playerToLose_info['player_color']),
			'playerToLose_prevNrDice' => $playerToLose_info['player_dice_nr'],
			'playerToLose_die' => $playerToLose_info['player_die1'],
        ) );

        // Notify all players about the die loss
        self::notifyAllPlayers( "dieLoss", $msgLoss, array(
            'player_name' => self::getActivePlayerName(),
            'playerToLose_name' => $this->getPlayerNameWithColor($playerToLose_info['player_name'], $playerToLose_info['player_color']),
        ) );

		self::setGameStateValue( 'playerWhoLostId', $playerToLose_id);

        $this->gamestate->nextState( "doubt" );
    }

    function calza()
    {
        $this->gamestate->checkPossibleAction( "calza" );

        $player_id = self::getCurrentPlayerId();

        $sql = "SELECT player_dice_nr, player_die1, player_name FROM player WHERE player_id = $player_id";
        $playerDice = self::getObjectFromDB( $sql );

        $diceNr = $playerDice["player_dice_nr"];
        $playerName = $playerDice["player_name"];

        $calzaPlayers = $this->getCalzaPlayers();

        if (!in_array($player_id, $calzaPlayers)) //player to bid - this player could not be able to call calza.
        {
            throw new BgaUserException( self::_("You cannot call calza at this moment!") );
        }

        self::incStat(1, "number_of_calza_calls", $player_id);
        self::incStat(1, "ended_with_calza");

        $lastBid = $this->getLastBid();

        $nr = $this->checkTotalDiceNumber($lastBid);

        $calzaIsSuccess = false;

        if ($nr != $lastBid["value"])
        {
            $msg = clienttranslate( '${player_name} called calza and failed.');
            $msgDie = clienttranslate( '${player_name} loses a die');

            $sql = "update player set isEliminated = if (player_dice_nr = 1, 1, 0)";
            $sql .= ", player_dice_nr = player_dice_nr - 1 where player_id = ".$player_id;

        } else
        {
            $msg = clienttranslate('${player_name} called calza and succeeded.');
            if ($diceNr < 5)
                $msgDie = clienttranslate('${player_name} gains a die');
            else
                $msgDie = clienttranslate('${player_name} have all the dice, so doesn\'t gain one');
            self::incStat(1, "number_of_successful_calza_calls", $player_id);
            $calzaIsSuccess = true;

            if ($diceNr < 5)
                $sql = "update player set player_dice_nr = player_dice_nr + 1 where player_id = ".$player_id;
        }

        self::DbQuery($sql);

        self::setGameStateValue('isCalzaSuccess', $calzaIsSuccess ? 1 : 0);
        self::setGameStateValue('lastCall', self::CALL_CALZA);

        // Notify all players about the bid
        self::notifyAllPlayers( "calza", $msg, array(
            'player_id' => $player_id,
            'player_name' => $playerName,
            'isSuccess' => $calzaIsSuccess,
            'diceNr' => $diceNr,
            'die' => $playerDice["player_die1"]
        ) );

        self::notifyAllPlayers( "dieChange", $msgDie, array(
            'player_name' => $playerName,
        ) );

        self::setGameStateValue( 'playerWhoLostId', $player_id);

        $this->gamestate->nextState( "calza" );
    }


//****************************************************************************************
//**                 Game state arguments
//****************************************************************************************

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */

	function argPlayerTurn()
    {
        $currentPlayerId = self::getActivePlayerId();

        $isCalza = self::getGameStateValue('calzaVariant') == self::CALZA_YES;

        $args = array(
            "minimumBids" => $this->getMinimumBids(),
            "maximumBids" => $this->getMaximumBids(),
            "activePlayerId" => $currentPlayerId,
            "isCalza" => $isCalza,
        );

        if ($isCalza) {
            $activePlayersIds = $this->getCalzaPlayers();
            $args["calzaPlayers"] = $activePlayersIds;
        }
        return $args;
    }

	function argNewRound()
    {
		return array(
			"totalNumberOfDiceEstimate" => $this->getDiceNrEstimate(),
            "roundType" => self::getGameStateValue('currentRoundType'),
			);
    }

//****************************************************************************************
//**                 Game state actions
//****************************************************************************************

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */

     function stNewRound()
     {
	 	self::incStat( 1, "rounds_number" );

		$players = self::loadPlayersBasicInfos();

        //Roll dice
        foreach( $players as $player_id => $player )
        {
            $playerState = $this->initPlayer($player_id);

			// Notify player about his dice
            self::notifyPlayer( $player_id, 'newRound', clienttranslate('New round starts'), $playerState );
        }

		self::setGameStateValue( 'playerWhoLostId', 0);
		self::setGameStateValue( 'currentBid', 0);
		self::setGameStateValue( 'currentBidPlayerId', 0);
        self::setGameStateValue( 'palificoDieFace', 0);
        self::setGameStateValue( 'lastCall', -1);
        self::setGameStateValue( 'isCalzaSuccess', 0);

        $activePlayerId = $this->getActivePlayerId();
        self::incStat( 1, "turns_number", $activePlayerId );

        if (self::getGameStateValue('currentRoundType') == self::PALIFICO_ROUND)
            self::notifyAllPlayers( "palificoRound", clienttranslate("This is a palifico round."), array());

		$this->gamestate->nextState( "" );
     }

	 function stNextPlayer()
     {
        $this->skipEliminatedAndActivate();

        $currentPlayerId = $this->getActivePlayerId();
        

        self::incStat( 1, "turns_number", $currentPlayerId );
        $this-> giveExtraTime($currentPlayerId);

        $this->gamestate->nextState( "nextPlayer" );
     }

	 function stEndRound()
	 {
		$nextPlayer = self::getGameStateValue( 'playerWhoLostId');
        $lastCall = self::getGameStateValue( 'lastCall');

		$sql = 'select player_name, isEliminated from player where player_id = '.$nextPlayer;
        $player = self::getObjectFromDB( $sql );
        $isEliminated = (int)$player['isEliminated'];

        //if ($isEliminated)
        //    self::notifyAllPlayers( "eliminated", clienttranslate('${player_name} is eliminated.'), array(
        //        'player_name' => $player['player_name']
        //    ) );

        $table = $this->generateScoreTable($nextPlayer);
        $this->notifyAllPlayers( "tableWindow", '', array(
            "id" => 'roundScoring',
            "title" => clienttranslate("Round overview"),
            "table" => $table,
            "header" => '',
            "footer" => '',
            "closelabel" => clienttranslate('Close')
        ) );

        $this->gamestate->changeActivePlayer( $nextPlayer );

        $this->eliminateZombies();

        if ($isEliminated != 0)
		{
//            self::DbQuery( "UPDATE player SET player_score=player_score+1 WHERE isEliminated = 0" );
//
//            $sql = "SELECT player_id FROM player WHERE isEliminated = 0";
//            $activePlayers = self::getCollectionFromDb( $sql );
//            self::notifyAllPlayers( "score", '', array(
//                'eliminatedPlayerId'=>$nextPlayer,
//                'activePlayers'=>$activePlayers,
//            ) );

            $this->completeElimination($nextPlayer);

            //needed for the skipEliminatedAndActivate because it will start from player who bid last
            self::setGameStateValue( 'currentBidPlayerId', $nextPlayer);
            if (!$this->skipEliminatedAndActivate())
            {
                //for delay after showing the last score dialog
                $this->notifyAllPlayers( "endOfGame", '', array() );

                $this->gamestate->nextState( "endGame" );
                return;

            }

            self::eliminatePlayer( $nextPlayer );

        }

         //determine if this round is palifico
         $activePlayer_id = $this->getActivePlayerId();
         $sql = 'select player_dice_nr from player where player_id = '.$activePlayer_id;
         $activePlayer = self::getObjectFromDB( $sql );

         //TODO - see if the round is palifico if a player didn't just loose a die (i.e. the player before him was eliminated)
         if ($activePlayer['player_dice_nr'] == 1 && $nextPlayer == $activePlayer_id)
             self::setGameStateValue('currentRoundType', self::PALIFICO_ROUND);
         else
             self::setGameStateValue('currentRoundType', self::NORMAL_ROUND);

        $this->gamestate->nextState( "nextRound" );
	 }

//****************************************************************************************
//**                 Zombie
//****************************************************************************************

    /*
        zombieTurn:

        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */

    function zombieTurn( $state, $active_player )
    {
    	$statename = $state['name'];
    	
        if ($state['type'] == "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState( "zombiePass" );
                	break;
            }

            return;
        }

        if ($state['type'] == "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $sql = "
                UPDATE  player
                SET     player_is_multiactive = 0
                WHERE   player_id = $active_player
            ";
            self::DbQuery( $sql );

            $this->gamestate->updateMultiactiveOrNextState( '' );
            return;
        }

        throw new feException( "Zombie mode not supported at this game state: ".$statename )  ;  
    
 /*       $statename = $state['name'];

        if ($state['type'] = "activeplayer"){

            //check if only one active nonzombie player
            $sql = "SELECT count(*) FROM player WHERE player_zombie = 0 AND isEliminated = 0";
            $nr = self::getUniqueValueFromDB($sql);

            self::setGameStateValue( 'currentBidPlayerId', $active_player);

            if ($nr > 1) {
                $this->gamestate->nextState( "zombiepass" );
            } else {
                $this->gamestate->nextState( "zombieend" );
            }

            return;
        }

         //throw new feException( "Zombie mode not supported at this game state: ".$statename );
         
         */
    }
}
