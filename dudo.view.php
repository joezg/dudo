<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Dudo implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * dudo.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in dudo_dudo.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
  require_once( APP_BASE_PATH."view/common/game.view.php" );
  
  class view_dudo_dudo extends game_view
  {
    const TOP_ROW_CLASS = "top_row_die";
    const BOTTOM_ROW_CLASS = "bottom_row_die";
    const LEFT_COLUMN_CLASS = "left_column_die";
    const MIDDLE_COLUMN_CLASS = "middle_column_die";
    const RIGHT_COLUMN_CLASS = "right_column_die";
  
    function getGameName() {
        return "dudo";
    }    
  	function build_page( $viewArgs )
  	{		
  	    // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count( $players );

        global $g_user;
        $player_color = '000000';
        if( isset( $players[ $g_user->get_id() ] ) )
            $player_color = $players[ $g_user->get_id() ]['player_color'];

        /*********** Place your code below:  ************/
        
        // Arrange players so that I am on bottom left position
        $player_to_dir = $this->game->getPlayersToDirection();

        $this->page->begin_block( "dudo_dudo", "die" );
        for ($i = 1; $i <= 5; $i++)
        {
            
            switch ($i) {
            case 1:
                $row_class = self::TOP_ROW_CLASS;
                $column_class = self::LEFT_COLUMN_CLASS;
                break;
            case 2:
                $row_class = self::TOP_ROW_CLASS;
                $column_class = self::MIDDLE_COLUMN_CLASS;
                break;
            case 3:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::LEFT_COLUMN_CLASS;
                break;
            case 4:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::MIDDLE_COLUMN_CLASS;
                break;
            case 5:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::RIGHT_COLUMN_CLASS;
                break;
			}
        
            $this->page->insert_block( "die", array( "I" => $i,
                                                     "ROW" => $row_class,
                                                     "COLOR" => $this->adjustDotsColor($player_color),
                                                     "COLUMN" => $column_class ) );
        }
        
        $this->page->begin_block( "dudo_dudo", "player" );
        foreach( $player_to_dir as $player_id => $dir )
        {
            $this->page->insert_block( "player", array( "PLAYER_ID" => $player_id,
                                                        "PLAYER_NAME" => $players[$player_id]['player_name'],
                                                        "COLOR" => $this->adjustDotsColor($players[$player_id]['player_color']),
                                                        "PLAYER_COLOR" => $players[$player_id]['player_color'],
                                                        "DIR" => $dir ) );
        }
        
		$this->page->begin_block( "dudo_dudo", "die_bid" );
		for ($i = 1; $i <= 6; $i++)
        {
            
            switch ($i) {
            case 1:
                $row_class = self::TOP_ROW_CLASS;
                $column_class = self::LEFT_COLUMN_CLASS;
				$dots_class = "one";
                break;
            case 2:
                $row_class = self::TOP_ROW_CLASS;
                $column_class = self::MIDDLE_COLUMN_CLASS;
				$dots_class = "two";
                break;
            case 3:
                $row_class = self::TOP_ROW_CLASS;
                $column_class = self::RIGHT_COLUMN_CLASS;
				$dots_class = "three";
                break;
            case 4:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::LEFT_COLUMN_CLASS;
				$dots_class = "four";
                break;
            case 5:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::MIDDLE_COLUMN_CLASS;
				$dots_class = "five";
                break;
			case 6:
                $row_class = self::BOTTOM_ROW_CLASS;
                $column_class = self::RIGHT_COLUMN_CLASS;
				$dots_class = "six";
                break;
			}
        
            $this->page->insert_block( "die_bid", array( "I" => $i,
                                                     "ROW" => $row_class,
                                                     "COLUMN" => $column_class,
                                                     "COLOR" => $this->adjustDotsColor($player_color),
													 "DOTS" => $dots_class) );
        }
		
        $this->tpl['MY_CUP'] = self::_("My cup");
		
		$this->tpl['PLACE_BID'] = self::_("Place your bid");

        $this->tpl['PALIFICO'] = self::_("palifico!");
		
		$this->tpl['PLEASE_WAIT'] = self::_("Please wait for your turn");

        /*
        
        // Examples: set the value of some element defined in your tpl file like this: {MY_VARIABLE_ELEMENT}

        // Display a specific number / string
        $this->tpl['MY_VARIABLE_ELEMENT'] = $number_to_display;

        // Display a string to be translated in all languages: 
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::_("A string to be translated");

        // Display some HTML content of your own:
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::raw( $some_html_code );
        
        */
        
        /*
        
        // Example: display a specific HTML block for each player in this game.
        // (note: the block is defined in your .tpl file like this:
        //      <!-- BEGIN myblock --> 
        //          ... my HTML code ...
        //      <!-- END myblock --> 
        

        $this->page->begin_block( "dudo_dudo", "myblock" );
        foreach( $players as $player )
        {
            $this->page->insert_block( "myblock", array( 
                                                    "PLAYER_NAME" => $player['player_name'],
                                                    "SOME_VARIABLE" => $some_value
                                                    ...
                                                     ) );
        }
        
        */



        /*********** Do not change anything below this line  ************/
  	}

    function adjustDotsColor($playerColor){
        $adjusted = $playerColor;

        switch ($playerColor){
            case "colors_with_black_dots":
                break;
            default:
                $adjusted .= ' white-dots';
        }

        return $adjusted;
    }
  }
  

