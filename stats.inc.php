<?php

/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Dudo implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * stats.inc.php
 *
 * Dudo game statistics description
 *
 */

/*
    In this file, you are describing game statistics, that will be displayed at the end of the
    game.
    
    There are 2 types of statistics:
    _ table statistics, that are not associated to a specific player (ie: 1 value for each game).
    _ player statistics, that are associated to each players (ie: 1 value for each player in the game).

    Statistics types can be "int" for integer, and "float" for floating point values.
    
    Once you defined your statistics there, you can start using "initStat", "setStat" and "incStat" method
    in your game logic, using statistics names defined below.
    
    !! It is not a good idea to modify this file when a game is running !!
*/

$stats_type = array(

    // Statistics global to table
    "table" => array(

        "rounds_number" => array("id"=> 10,
                    "name" => totranslate("Number of rounds"),
                    "type" => "int" ),
        "ended_with_dudo" => array("id"=> 11,
            "name" => totranslate("Number of rounds ended with dudo"),
            "type" => "int" ),
        "ended_with_calza" => array("id"=> 12,
            "name" => totranslate("Number of rounds ended with calza"),
            "type" => "int" ),

/*
        Examples:


        "table_teststat1" => array(   "id"=> 10,
                                "name" => totranslate("table test stat 1"), 
                                "type" => "int" ),
                                
        "table_teststat2" => array(   "id"=> 11,
                                "name" => totranslate("table test stat 2"), 
                                "type" => "float" )
*/  
    ),
    
    // Statistics existing for each player
    "player" => array(

        "rounds_number" => array("id"=> 10,
            "name" => totranslate("Number of rounds"),
            "type" => "int" ),
        "turns_number" => array("id"=> 11,
                    "name" => totranslate("Number of turns"),
                    "type" => "int" ),
        "number_of_dice_rolled" => array("id"=> 12,
            "name" => totranslate("Number of dice rolled"),
            "type" => "int" ),
        "number_of_wild_dice_rolled" => array("id"=> 13, 
            "name" => totranslate("Number of paco dice rolled"),
            "type" => "int" ),
        "number_of_dudo_calls" => array("id"=> 14,
            "name" => totranslate("Number of dudo calls"),
            "type" => "int" ),
        "number_of_successful_dudo_calls" => array("id"=> 15,
            "name" => totranslate("Number of successful dudo calls"),
            "type" => "int" ),
        "number_of_calza_calls" => array("id"=> 16,
            "name" => totranslate("Number of calza calls"),
            "type" => "int" ),
        "number_of_successful_calza_calls" => array("id"=> 17,
            "name" => totranslate("Number of successful calza calls"),
            "type" => "int" ),
        "rounds_with_second_bid" => array("id"=> 18,
            "name" => totranslate("Number of rounds in which player had two or more bids"),
            "type" => "int" ),
/*
        Examples:    
        
        
        "player_teststat1" => array(   "id"=> 10,
                                "name" => totranslate("player test stat 1"), 
                                "type" => "int" ),
                                
        "player_teststat2" => array(   "id"=> 11,
                                "name" => totranslate("player test stat 2"), 
                                "type" => "float" )

*/    
    )

);
