/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Dudo implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * dudo.js
 *
 * Dudo user interface script
 * 
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */
"use strict";
define([
    "dojo","dojo/_base/declare", "dojo/NodeList-traverse",
    "dojo/NodeList-dom", "dojo/_base/lang", "dojo/dom-attr",
    "ebg/core/gamegui",
    "ebg/counter"
],
function (dojo, declare, traverse, dom, lang, attr) {
    return declare("bgagame.dudo", ebg.core.gamegui, {
        constructor: function(){
			
			//**initializing variables 
            this.currentBid = {die:0, value:0};
			this.nextBid = {die:0, value:0};
        },
        
        nextBid: null, //A bid that is about to be sent to server
		currentBid: null, //Currently last bid in the auction.
		totalNrDiceEstimate: 0, //Because total dice left should be unknown 
								//this is only estimate returned by the server
		isUserSetNumber: false, //When user sets number for the bid it'll not 
								//be changed when choosing a die face except in
								//case where the number set is lower than the 
								//minimum number allowed.
		minimumBids: null, //Minimum bids for all die faces returned from server
        maximumBids: null, //Minimum bids for all die faces returned from server
        isPalifico: false, //Indicates whether this round is palifico round
        
		/*
            setup:
            
            This method must set up the game user interface according to current game situation specified
            in parameters.
            
            The method is called each time the game interface is displayed to a player, ie:
            _ when the game starts
            _ when a player refreshes the game page (F5)
            
            "gamedatas" argument contains all data retrieved by your "getAllDatas" PHP method.
        */
        setup: function( gamedatas )
        {
            this.currentBid = this.parseBid(gamedatas.currentBid);

            // Setting up player boards by putting corresponding player's last bid in his box
            for( var player_id in gamedatas.players )
            {
                var player = gamedatas.players[player_id];
                var playerLastBid = player.player_last_bid;
                
                if (playerLastBid == null) //hide bid if no bid
                {
                    dojo.addClass('playertablebid_'+player_id, 'hidden');
                }
                else //set and show bid
                {
					var bid = this.parseBid(parseInt(playerLastBid));

                    var isCurrent = false;
                    if (bid.value == this.currentBid.value &&
                        bid.die == this.currentBid.die)
                        isCurrent = true;
					this.setBid(player_id, bid.die, bid.value, isCurrent);
                }

                if (player.isEliminated == 1)
                {
                    this.disablePlayerPanel( player_id );
                    var playerPanel = dojo.query("#playertablebid_" + player.id).closest(".playertable");

                    playerPanel.forEach(function(node, index, array){
                        dojo.addClass(node, "inactive");
                    });


                }

                $('dice_number_'+player.id).innerHTML = player.player_dice_nr;
            }
            
            // setting current player dice
            for( var dieId in this.gamedatas.currentPlayerDice )
            {
                var die = this.gamedatas.currentPlayerDice[dieId];
                
                var dotsClass = '';
                
                if (die == null) //die is hidden if player doesn't have it
                {
                    dojo.addClass( dieId, 'hidden');
                }
                else //die face is set and die is shown
                {
                    dojo.removeClass( dieId, 'hidden');
                
                    die = parseInt(die);
                
                    this.setDie(die, dieId);    
                }
            }
			
			//getting other data from server
			this.totalNrDiceEstimate = this.gamedatas.totalNumberOfDiceEstimate;
			this.currentBid = this.parseBid(gamedatas.currentBid);
			this.minimumBids = gamedatas.minimumBids;
            this.maximumBids = gamedatas.maximumBids;
			this.isUserSetNumber = false;
            this.isPalifico = gamedatas.roundType == 1;

            this.setupBiddingBox();

            // Setup game notifications to handle (see "setupNotifications" method below)
            this.setupNotifications();

			//connecting elements of UI to handlers, for user input
			dojo.query('#bidding_box .die').connect('onclick', this, 'onDieBidClick');
			dojo.query('#bidding_box div[class*="arrow-"]').connect('onclick', this, 'onBidArrow');

            this.addTooltipToClass( 'nr-dice-tooltip', _("Number of dice left"), _(""),0);
        },
       
        //************************************************************************************************
        //** Game & client states
        //************************************************************************************************
		
		// onEnteringState: this method is called each time we are entering into a new game state.
        //                  This method is used to perform some user interface changes at this moment.
        //
        onEnteringState: function( stateName, args )
        {
            console.log( 'Entering state: '+stateName );
            
            switch( stateName )
            {
            case 'firstPlayerTurn':
			case 'playerTurn':
            	//minimum bids for this turn are received.
                this.minimumBids = args.args.minimumBids;
                this.maximumBids = args.args.maximumBids;
				//variables are reset
				this.nextBid = {die:0, value:0};
				this.isUserSetNumber = false;
				this.selectDie(null);
                this.setupBiddingBox();

                this.hideOrShowBiddingbox();

                break;
           case 'newRound':
				this.currentBid = {die:0, value:0};
				this.totalNrDiceEstimate = args.args.totalNumberOfDiceEstimate;
                //is palifico round is determined
                this.isPalifico = args.args.roundType == 1;
                if (this.isPalifico)
                    this.showMessage(_("This is a palifico round."), "info");
				break;
            case 'dummmy':
                break;
            }
        },

        // onLeavingState: this method is called each time we are leaving a game state.
        //                 This method is used to perform some user interface changes at this moment.
        //
        onLeavingState: function( stateName )
        {
            console.log( 'Leaving state: '+stateName );
            
            switch( stateName )
            {
			case 'dummmy':
                break;
            }               
        }, 

        // onUpdateActionButtons: in this method "action buttons" that are displayed in the action status
        //                        bar (ie: the HTML links in the status bar) are managed.
        //        
        onUpdateActionButtons: function( stateName, args )
        {
            if( this.isCurrentPlayerActive() )
            {
                switch( stateName )
                {
					case 'playerTurn':
                        this.addActionButton( 'bid_button', _('place bid'), 'onBid' );
                        this.addActionButton( 'doubt_button', _('call dudo'), 'onDoubt' );
                        //if (this.gamedatas.currentPlayerDice.die_5 == null && args.isCalza)
                        if (args.isCalza)
                                this.addActionButton( 'calza_button', _('call calza'), 'onCalza' );
                        break;
						
					case 'firstPlayerTurn':
						this.addActionButton( 'bid_button', _('place bid'), 'onBid' );
						break;
                }
            } else if (stateName == "playerTurn" && args.isCalza) {

                var thisRef = this;
                args.calzaPlayers.forEach(function(calzaPlayerId) {
                    if (thisRef.player_id == calzaPlayerId)
                        thisRef.addActionButton( 'calza_button', _('You can call calza'), 'onCalza' );
                });
            }

        },        

        //*****************************************************************************************************
        //** Utility methods
        //*****************************************************************************************************
        
		//This method shows or hides bidding-box in regard whether player is active or not. It also sets initial 
		//value for the next bid value according to the currently last bid in the round.
		hideOrShowBiddingbox: function()
		{
            if (this.isCurrentPlayerActive()){
				dojo.removeClass("biddingbox-bid", 'display-none');
				dojo.addClass("biddingbox-wait", 'display-none');

                //setting up initial bid value
                if (this.isPalifico && this.currentBid.value > 0)
                    this.setNextBidValue(parseInt(this.currentBid.value) + 1)
				else if (this.currentBid.value > 0 && this.currentBid.die > 1) {
				//If current bid exists and is not for die face 1, minimum value for non-one die face is set
					if (this.currentBid.die < 6) 
						this.setNextBidValue(this.currentBid.value);
					else 
						this.setNextBidValue(parseInt(this.currentBid.value) + 1);
				} else if (this.currentBid.die == 1)
				//If current bid exists and is for die face 1, minimum value for die face 1 is set
					this.setNextBidValue(parseInt(this.currentBid.value) + 1);
				else { 
				//if current bid not exists (i.e. first bid in a round) value is set to third of the total 
				//number of dice
                    var div = 3;
                    if (this.isPalifico)
                        div = 6;

					this.setNextBidValue(Math.floor(this.totalNrDiceEstimate/div));
					this.isUserSetNumber = true;
				}

                if (this.isPalifico){
                    dojo.removeClass('palifico_round_info', 'display-none');
                } else {
                    dojo.addClass('palifico_round_info', 'display-none');
                }
			}
			else {
				dojo.addClass('biddingbox-bid', 'display-none');
				dojo.removeClass('biddingbox-wait', 'display-none');
			}
		},

        selectBidDie: function (dieNode) {
            var die = dojo.query(dieNode).closest('.die');
            //this.selectDie(dieNode);
            this.selectDie(die);

            if (dojo.hasClass(dieNode, 'die_one'))
                this.nextBid.die = 1;

            if (dojo.hasClass(dieNode, 'die_two'))
                this.nextBid.die = 2;

            if (dojo.hasClass(dieNode, 'die_three'))
                this.nextBid.die = 3;

            if (dojo.hasClass(dieNode, 'die_four'))
                this.nextBid.die = 4;

            if (dojo.hasClass(dieNode, 'die_five'))
                this.nextBid.die = 5;

            if (dojo.hasClass(dieNode, 'die_six'))
                this.nextBid.die = 6;
        },

        setupBiddingBox: function()
        {
          var curBid = this.currentBid;

          for (var i=1; i<=6; i++){
              if (this.getMinimumValue(i) == -1)
                  dojo.query("#die_bid_" + i).addClass("die-bid-disabled");
              else
                  dojo.query("#die_bid_" + i).removeClass("die-bid-disabled");

              if (this.currentBid.die == i && this.isPalifico)
                this.selectBidDie(dojo.query('#die_bid_' + i).children()[0]);
          }

          //TODO remove lines below when tested

          return ;
//          if (this.currentBid.die > 0 && this.isPalifico)
//          {
//              var context = this;
//              dojo.query("#die_bid > .die").forEach(function(node, index, array){
//                  if (dojo.attr(node, "id") != "die_bid_" + curBid.die)
//                    dojo.addClass(node, "die-bid-disabled");
//                  else {
//                    var dieNode = dojo.query(node).children()[0];
//                    context.selectBidDie(dieNode);
//                  }
//              });
//          } else {
//              dojo.query("#die_bid > .die").removeClass("die-bid-disabled");
//
//              if (curBid.die == 0 && curBid.value == 0 && !this.isPalifico){
//                  dojo.query("#die_bid_1").addClass("die-bid-disabled");
//              }
//
//          }
        },
		
		//This method change the next bid value in UI and in the backing field. It also makes the down arrow inactive when
		//the minimum value for chosen die face is set.
		setNextBidValue: function(value)
		{
			dojo.byId('number_bid').innerHTML = value;
			this.nextBid.value = value;

            var minValue = this.getMinimumValue(this.nextBid.die);
            var maxValue = this.getMaximumValue(this.nextBid.die);

            if (minValue == -1 && this.nextBid.die == 0) {
                minValue = 1;
            }

            if (value == minValue){
				dojo.addClass("arrow_down", "inactive");
			}
			else {
				dojo.removeClass("arrow_down", "inactive");
			}

            if (value == maxValue){
                dojo.addClass("arrow_up", "inactive");
            }
            else {
                dojo.removeClass("arrow_up", "inactive");
            }
		},
		
		//This method parses the bid from integer to die/value object.
		parseBid: function(bid)
		{
			var number = bid % 10;
            var level = Math.floor(bid / 10);
		
			var retValue = { die: number, value: level };

			return retValue;
		},
		
		//sets the bid for a player
		//called when player makes a bid to show it in the user interface 
		//or when refreshing the UI with F5
		setBid: function( playerId, die, value, isCurrent )
		{
			dojo.removeClass('playertablebid_'+playerId, 'hidden');
			
            if (isCurrent)
            {
                //sets the opacity
                dojo.query('.current_bid').removeClass('current_bid');
                dojo.addClass('playertablebid_'+playerId, 'current_bid');
            }
            this.setDie(die, 'bidding_die_'+playerId);
            $('bidding_bid_'+playerId).innerHTML = value;
			
			if (value > 9)
				dojo.addClass('bidding_bid_'+playerId, 'bid-more-space');
			else
				dojo.removeClass('bidding_bid_'+playerId, 'bid-more-space');
		},
		
		//Gets the CSS class for dots for the die with provided die face
		getDotsClass: function(dieFace)
		{
			var dotsClass;
			switch(parseInt(dieFace))
            {
				case 1:
					dotsClass = 'die_one';
                    break;
                case 2:
                    dotsClass = 'die_two';
                    break;
                case 3:
                    dotsClass = 'die_three';
                    break;
                case 4:
                    dotsClass = 'die_four';
                    break;
                case 5:
                    dotsClass = 'die_five';
                    break;
                case 6:
                    dotsClass = 'die_six';
                    break;
                default:
                    dotsClass = '';
            }
			
			return dotsClass;
		},
        
		//Sets any die to corresponding face. Dice in players' bids, Dice in 
		//current player's cup or dice in bidding-box.
        setDie: function( numberOfDots, dieId )
        {
			var bidDots = dojo.query( '#'+dieId+' .dots');
			bidDots.removeClass();
			bidDots.addClass('dots');
			
			var dotsClass = this.getDotsClass(numberOfDots);
			
            dojo.query( '#'+dieId+' .dots').addClass(dotsClass);
        },
		
		//gets the minimum value for a bid in provided die face
		getMinimumValue: function( die )
		{
			var min;
		
			if (die > 0)
				min = this.minimumBids[die];
			else
				min =  this.minimumBids[1];
				
			if (min < 1 || typeof min == 'undefined')
                return -1;
				//min = 1;
				
			return min;
			//return 0;
		},

        //gets the minimum value for a bid in provided die face
        getMaximumValue: function( die )
        {
            var max;

            if (die > 0)
                max = this.maximumBids[die];
            else
                max =  this.maximumBids[1];

            if (max < 1 || typeof max == 'undefined' || max > 100)
                return 100;

            return max;
        },
		
		selectDie: function(die)
		{
			dojo.query('.die_bid_selected').removeClass('die_bid_selected');
			
			if (null != die)
				dojo.query(die).addClass('die_bid_selected');
		},

        getDieColor: function(playerId)
        {
            var color = this.gamedatas.players[playerId].color;

            switch (color){
                case "color_with_black_dot": //ie ffff00
                    break;
                default:
                    color += ' white-dots';
            }
            return color;
        },
        
		//****************************************************************************************
        //** Player's action
        //****************************************************************************************
		/*
        
            Here, the methods to handle player's action (ex: results of mouse click on game 
			objects) are defined.
            
            Most of the time, these methods:
            _ check the action is possible at this game state.
            _ make a call to the game server
        
        */

        //When die in bidding-box is clicked it is highlighted and the value of a bid is adjusted
        //except in the case that the value is set before clicking the die face. Value is set to the
        //minimum value for given die face.
		onDieBidClick: function(e)
		{
            var isDisabled = false;
            dojo.query(e.target).parent().forEach(function(node, index, array){
                if (dojo.hasClass(node, 'die-bid-disabled'))
                    isDisabled = true;
            });

            if (isDisabled)
                return;

            this.selectBidDie(e.target);

            var minValue = this.getMinimumValue(this.nextBid.die);
            var maxValue = this.getMaximumValue(this.nextBid.die);
			
			if (!this.isUserSetNumber || this.nextBid.value <= minValue) 
			{
					this.setNextBidValue(minValue);
					this.isUserSetNumber = false;
			} else if (!this.isUserSetNumber || this.nextBid.value >= maxValue) {
                this.setNextBidValue(maxValue);
                this.isUserSetNumber = false;
            } else
                this.setNextBidValue(this.nextBid.value);
		},
        
		//This is called when user clicks on doubt action in the action bar.
		onDoubt: function()
		{
			console.log('onDoubt - enter');
			
			if (this.checkAction('doubt'))
			{
				this.ajaxcall( '/dudo/dudo/doubt.html', 
					{ lock: true
					}, 
					this, function( result ) {
					}
				);
			}
		},

        //This is called when user clicks on calza action in the action bar.
        onCalza: function()
        {
            console.log('onCalza - enter');

            if (this.checkPossibleActions('calza'))
            {
                this.ajaxcall( '/dudo/dudo/calza.html',
                    { lock: true
                    },
                    this, function( result ) {
                    }
                );
            }

        },
		
		//This is called when user clicks "place a bid" in the action bar.
		//It checks if bid is complete and whether active user can make that action
		//at the moment.
		onBid: function()
		{
			// Preventing default browser reaction
            //dojo.stopEvent( evt );

            if (this.nextBid.die <= 0) {
				this.showMessage( _('You must pick a die'), 'error' )
				return;
			}
			
			if (this.nextBid.value <= 0) {
				this.showMessage( _('You must pick number of dice lager than 0'), 'error' )
				return;
			}
			
			if (this.checkAction('bid'))
			{
				this.ajaxcall( '/dudo/dudo/bid.html', 
					{ lock: true, 
					  bidDie: this.nextBid.die, 
					  bidValue: this.nextBid.value
					}, 
					this, function( result ) {
					}
				);
			}
		},
		
		//This is called when user clicks on bidding-box arrows.
		//If clicking down arrow, it is checked whether new value
		//will be at least a minimum value for given die face.
		onBidArrow: function(e, sender)
		{
            var num = parseInt(dojo.byId('number_bid').innerHTML);
			if (dojo.hasClass(e.target, 'arrow-up')) 
			{
                var maxValue = this.getMaximumValue(this.nextBid.die);
				if (num +1 <= maxValue)
                    this.setNextBidValue(num + 1);
			}
			else
			{
                if (this.nextBid.die > 0)
				{
                    var minValue = this.getMinimumValue(this.nextBid.die);
					if (num - 1 >= minValue && minValue > -1)
						this.setNextBidValue(num - 1);
				} else {
                    var minValue = this.getMinimumValue(1);
                    if (minValue == -1)
                        minValue = 1;

					if (num - 1 >= minValue)
						this.setNextBidValue(num - 1);
				}
				
			}
			this.isUserSetNumber = true;
		},
		
        //********************************************************************************************
        //** Reaction to cometD notifications
		//********************************************************************************************
        
		/*
            setupNotifications:
            In this method, game notifications are associated with local methods to handle them.
            
            Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
                  gameName.game.php file.
        
        */
        setupNotifications: function()
        {
            dojo.subscribe( 'newRound', this, "notif_newRound" );
			dojo.subscribe( 'bid', this, "notif_bid" );
			dojo.subscribe( 'doubt', this, "notif_doubt" );
            this.notifqueue.setSynchronous( 'doubt', 1500 );
            dojo.subscribe( 'score', this, "notif_score" );
            dojo.subscribe( 'calza', this, "notif_calza" );
            this.notifqueue.setSynchronous( 'calza', 1500 );
            dojo.subscribe( 'endOfGame', this, "notif_endOfGame" );
            this.notifqueue.setSynchronous( 'endOfGame', 2500 );

			// Example 2: standard notification handling + tell the user interface to wait
            //            during 3 seconds after calling the method in order to let the players
            //            see what is happening in the game.
            // dojo.subscribe( 'cardPlayed', this, "notif_cardPlayed" );
            // this.notifqueue.setSynchronous( 'cardPlayed', 3000 );
            // 
        },  
        
        notif_newRound: function( notif )
        {
            console.log('###########Entering notif_newRound##########');
			// Setting up player boards
            for( var player_id in this.gamedatas.players )
            {
                var player = this.gamedatas.players[player_id];
                
				dojo.addClass('playertablebid_'+player_id, 'hidden');
            }
            
			for( var dieId in notif.args.dice )
            {
				
                var die = notif.args.dice[dieId];

				if (die == 'NULL')
                {
                    //dojo.addClass( dieId, 'hidden');
                }
                else
                {
                    dojo.removeClass( dieId, 'hidden');
                
                    die = parseInt(die);
			        this.setDie(die, dieId);    
                }
            }
			
			this.isUserSetNumber = false;
        },
		
		notif_bid: function( notif )
        {
            console.log('###########entering notif_bid##########');
			this.setBid(notif.args.player_id, parseInt(notif.args.face_die), notif.args.nr_dice, true);
			this.currentBid.die = notif.args.face_die;
			this.currentBid.value = notif.args.nr_dice;
			this.isUserSetNumber = false;
        },

        notif_score: function( notif )
        {
            console.log('###########entering notif_score##########');
            var eliminatedPlayer = notif.args.eliminatedPlayerId;
            var activePlayers = notif.args.activePlayers;

            var player_id;
            for (player_id in activePlayers){
                this.scoreCtrl[ player_id ].incValue( 1 );
            }

            this.disablePlayerPanel( eliminatedPlayer );
            var playerPanel = dojo.query("#playertablebid_" + eliminatedPlayer).closest(".playertable");

            playerPanel.forEach(function(node, index, array){
                dojo.addClass(node, "inactive");
            });

        },
		
		notif_doubt: function( notif )
        {
            var playerToLoseId = notif.args.playerToLose_id;

            console.log('###########entering notif_doubt##########');
            if (this.player_id == playerToLoseId)
            {
                var die = dojo.byId("die_" + notif.args.playerToLose_prevNrDice);

                //this.slideToObjectAndDestroy( die, 'usedDiceCup', 1000, 500 );
                var newDie = lang.clone(die);
                attr.set(newDie, "Id", "temp");
                dojo.addClass(die, "hidden");
                this.slideTemporaryObject( newDie, 'ebd-body', die, 'usedDiceCup', 1000, 500);
            } else
            {
                var dotsClass = this.getDotsClass(notif.args.playerToLose_die);

                this.slideTemporaryObject( this.format_block('jstpl_die', {dots_class: dotsClass, color: this.getDieColor(playerToLoseId)} ), 'ebd-body', 'overall_player_board_'+notif.args.playerToLose_id, 'usedDiceCup', 1000, 500);
            }

            $('dice_number_'+playerToLoseId).innerHTML = notif.args.playerToLose_prevNrDice - 1;

        },

        notif_calza: function( notif )
        {
            console.log('###########entering notif_calza##########');

            var isCalzaSuccess = notif.args.isSuccess;
            var newDiceNr = notif.args.diceNr;

            if (!isCalzaSuccess)
            {
                if (this.player_id == notif.args.player_id)
                {
                    var die = dojo.byId("die_" + notif.args.diceNr);

                    //this.slideToObjectAndDestroy( die, 'usedDiceCup', 1000, 500 );
                    var newDie = lang.clone(die);
                    attr.set(newDie, "Id", "temp");
                    dojo.addClass(die, "hidden");
                    this.slideTemporaryObject( newDie, 'ebd-body', die, 'usedDiceCup', 1000, 500);
                } else
                {
                    var dotsClass = this.getDotsClass(notif.args.die);

                    this.slideTemporaryObject( this.format_block('jstpl_die', {dots_class: dotsClass, color: this.getDieColor(notif.args.player_id)} ), 'ebd-body', 'overall_player_board_'+notif.args.player_id, 'usedDiceCup', 1000, 500);
                }

                newDiceNr--;
            } else
            {
                if (newDiceNr < 5)
                {
                    var dieValue = Math.floor((Math.random()*6)+1);
                    var dotsClass = this.getDotsClass(dieValue);

                    if (this.player_id == notif.args.player_id)
                    {
                        var nextInvisibleDie = dojo.query("#mycup .hidden")[0];
                        this.slideTemporaryObject( this.format_block('jstpl_die', {dots_class: dotsClass, color: this.getDieColor(this.player_id)} ), 'ebd-body', 'usedDiceCup', nextInvisibleDie, 1000, 500);
                    } else
                    {
                        this.slideTemporaryObject( this.format_block('jstpl_die', {dots_class: dotsClass, color: this.getDieColor(notif.args.player_id)} ), 'ebd-body', 'usedDiceCup', 'overall_player_board_'+notif.args.player_id, 1000, 500);
                    }
                    newDiceNr++;
                }
            }
            $('dice_number_'+notif.args.player_id).innerHTML = newDiceNr;
        },
        notif_endOfGame: function ( notif ){
            //Nothing to do. Used only to delay the game end.
        }
   });             
});
